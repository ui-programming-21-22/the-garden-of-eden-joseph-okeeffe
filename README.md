# Final Project - Farming Game

## Ideas (Inspried by Stardew Valley)
- You click on a tool to equip it
- Once the correct tool is equipped, choose your action
- You have to gather seeds in bushes
- Walk over to some grass and press a button while you have the hoe equipped
- This will till the soil
- Click on the seeds you have collected and press a button to plant them
- Gather water using the watering can by the pond
- Walk over to planted crops and press a button to water them
- Once watered enough, you can harvest them
- Have a day and night cycle (change the map)




## Features to Add
- [x] Draw the map on the canvas
- [x] Draw character on canvas
- [x] Get character moving and animating
- [x] Draw tool bar 
- [x] Make tool bar interactive
- [x] Show active tool on player
- [x] Add small animation for doing a task
- [x] Allow the user to collect water
- [x] Change Watering can sprite if its full or empty
- [x] Draw a grass sprite
- [x] Make grass sprite change if player interacts with it
- [x] Added the 4 different stages of grass/soil  <br><br>
-+-+- DONE by the 13th of March -+-+-
- [x] Harvest Crop 
- [x] Make grass return to normal
- [x] Show how much seeds / crops the user has
- [x] Allow user to go into house
- [x] Change the screen to display the inside of the house
- [x] Allow deposition of crops
- [x] Add info screen
- [x] Add day and night cycle
- [x] Allow user to earn coins
- [x] Add some delay between harvesting crops
- [x] Allow user to go to sleep
- [x] Setup the mobile playability factor (joystick, touch resposive)
- [ ] Add goal to the Game - Escape by day x or else you die! 
- [ ] Ask user to input name or other details
- [ ] Get local stoarge setup 




