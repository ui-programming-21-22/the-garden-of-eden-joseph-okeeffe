let gameVisibility = document.getElementById("game");
let formVisibility = document.getElementById("form");
let modalVisibility = document.getElementById("modal");



function checkVisibility()
{
    event.preventDefault();
    //let queryString = document.location.search;
    //var name = queryString.split("=")[1];

    let userName = document.forms["details"]["userName"].value;

    if (userName.length < 1)
    {
        console.log("Name");
        alert("Please Input a Name");
        modalVisibility.style.visibility = "hidden";
    }
    else
    {
        localStorage.setItem("userName", userName);    

        gameVisibility.style.visibility = "visible";
        formVisibility.style.visibility = "hidden";
        modalVisibility.style.visibility = "hidden";
    }
}

if(typeof(Storage) !== "undefined") 
{
    playerName = localStorage.getItem('userName');

    if(currentSeeds == 3)
    {
        localStorage.setItem("Seeds", currentSeeds);
    }

   // if(coins != 0)
   // {
        //localStorage.setItem("Coins", coins);
        
   // }

    if(currentSeeds > 0)
    {
        console.log("GET SEEDS");
        currentSeeds = localStorage.getItem("Seeds");
    }
                            
    if(currentCrops > 0)
    {
        console.log("GET CROPS");
        //currentCrops = localStorage.getItem("CurrentCrops");
    }
                            
    if(storedCrops > 0)
    {
        console.log("GET STORED CROPS");
        storedCrops = localStorage.getItem("StoredCrops");
    }                    
                                
    if (playerName)
    {
        console.log("found saved name: " + playerName);
        let form = document.forms["details"];
        form.style.display = "none";

        let modalContent = modal.children[0].children[2];
        modalVisibility.style.display = "block";
        modalContent.innerHTML = "Name: " + playerName + 
                                "<br> Seeds: " + currentSeeds +
                                "<br> Crops: " + currentCrops +
                                "<br> Coins: " + coins;
                                
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];

        validateButton.onclick = function()
        {
            modalVisibility.style.display = "none";
            gameVisibility.style.visibility = "visible";

            console.log("GET COINS");
            coins = localStorage.getItem("Coins");

            //coins = localStorage.getItem("Coins");
        }
     
        dismissButton.onclick = function()
        {
            localStorage.clear();
            modalVisibility.style.display = "none";
            form.style.display = "block";

            currentSeeds = 3;
            currentCrops = 0;
            storedCrops = 0;
            coins = 0;

        }
    }
    else
    {
        console.log("no data in localStorage, loading new session");
    }
  } 
else 
{
    console.log("Local storage is not supported.");
}
