const canvas = document.querySelector("#theCanvas");
const context = canvas.getContext("2d");

window.addEventListener("keydown", input);
window.addEventListener("keyup", input);
window.addEventListener("mousedown", input);
window.addEventListener("touchstart", input);

// IMAGES
let mapImage = new Image();
let spriteSheet = new Image();
let houseImage = new Image();
let busImage = new Image();

// PLAYER

const playerScale = 1.5;
let playerWidth = 36;
const playerHeight = 70;
const playerScaledWidth = playerWidth * playerScale;
const playerScaledHeight = playerHeight * playerScale;

let playerPosX = 1215;
let playerPosY = 160;
let playerInput = new PlayerInput("None");
let player = new GameObject(spriteSheet, playerPosX, playerPosY, playerWidth, playerHeight);

const mapScaleX = 1.8;
const mapScaleY = 1.35;
const mapWidth = 785;
const mapHeight = 441;
const mapScaledWidth = mapWidth * mapScaleX;
const mapScaledHeight = mapHeight * mapScaleY;


let shovel = false;
let hoe = false;
let scythe = false;
let wateringCan = false;
let wateringCanFull = false;
let seed = false;
let crop = false;
let hand = true;

let sleeping = false;

let canInteract = false;
let interacting = false;

let info = false;

let farmLand1Watered = false;
let farmLand2Watered = false;
let farmLand3Watered = false;

let toolPosX = 0;
let toolPosY = 0;

let toolWidthX = 0;
let toolWidthY = 0;
let toolOffsetX = 0;
let toolOffsetY = 0;

let currentSeeds = 3;
let currentCrops = 0;

let inside = false;

let storedCrops = 0;

let previousDay = 0;
let day = 1;

let coins = 0;

let escaped = false;

const coinsToWin = 20;
const timeToWin = 11;

let busTransition = mapScaledWidth;

function imageLoader()
{  
    mapImage.src = "assets/images/farmSpritesheet.png"
    spriteSheet.src = "assets/images/SpriteSheet.png"
    houseImage.src = "assets/images/insideHouse.png"
    busImage.src = "assets/images/bus.png"
}

imageLoader();
let gameCheck = document.getElementById("game");

function gameLoop()
{
    if(document.getElementById("game").style.visibility === "visible")
    {
        update();
        draw();
    }
    window.requestAnimationFrame(gameLoop);
}
let minutePlusser = 5;
window.requestAnimationFrame(gameLoop);

function update()
{
    farmLand1ScaledWidth = farmLand1Width * farmLandXScale;
    farmLand2ScaledWidth = farmLand2Width * farmLandXScale;
    farmLand3ScaledWidth = farmLand3Width * farmLandXScale;

    if(interacting == true)
    {
        interact();
    }
    else
    {
        move();
    }

    if(wateringCanFull == false)
    {
        toolBarYPos = 405;
    }
    if(wateringCanFull == true)
    {
        toolBarYPos = 464;
    }

    if(time.getMinutes() == 00 && sleeping == true)
    {
        minutePlusser = 60;
    }

    displayInteractMenu();

    context.fillText(coins, 130, 29);

    updateTime();

    if(escaped)
    {
        if(busTransition > 0)
        {
            busTransition -= 3;
        }
    }

}



let toolBarYPos = 405;
const toolBarScale = 1.5;
const toolBarWidth = 364;
const toolBarHeight = 52;
const toolBarScaledWidth = toolBarWidth * toolBarScale;
const toolBarScaledHeight = toolBarHeight * toolBarScale;

// FARM LAND 1
let farmLand1Width = 11;
let farmLand1FileXPos = 120;
let farmLand1FileYPos = 4;
let farmLand1XPos = 395.5;
let farmLand1YPos = 121;

// FARM LAND 2
let farmLand2Width = 11;
let farmLand2FileXPos = 120;
let farmLand2FileYPos = 4;
let farmLand2XPos = 229.5;
let farmLand2YPos = 121;

// FARM LAND 3
let farmLand3Width = 11;
let farmLand3FileXPos = 120;
let farmLand3FileYPos = 4;
let farmLand3XPos = 229.5;
let farmLand3YPos = 360;

// UNIVERSAL FARM LAND CONSTS 

const farmLandHeight = 119; // Height of sprite
const farmLandXScale = 1.8; // Scale of X
const farmLandYScale = 1.35; // Scale of Y
let farmLand1ScaledWidth = farmLand1Width * farmLandXScale;
let farmLand2ScaledWidth = farmLand2Width * farmLandXScale;
let farmLand3ScaledWidth = farmLand3Width * farmLandXScale;
const farmLandScaledHeight = farmLandHeight * farmLandYScale; // Scale * height

let mapYFilePos = 0;

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);

    if(!escaped)
    {
        // map
        if(inside == false)
        {
            context.drawImage(mapImage,
                                0,
                                mapYFilePos,
                                mapWidth,
                                mapHeight,
                                0,
                                0,
                                mapScaledWidth,
                                mapScaledHeight);
            displayFarmLand();                   
        }
        else
        {
            context.drawImage(houseImage,
                0,
                0,
                mapWidth,
                mapHeight,
                0,
                0,
                mapScaledWidth,
                mapScaledHeight);
        }

            // TOOLBAR
            context.drawImage(spriteSheet,
                0, // file posX
                toolBarYPos, // file posY
                toolBarWidth, // width of image
                toolBarHeight, // height of image
                470, // posX on screen
                595, // posY on screen
                toolBarScaledWidth, // SizeX of image                   
                toolBarScaledHeight); // sizeY of image

                    // TOOLBAR
            context.drawImage(spriteSheet,
                1, // file posX
                741, // file posY
                46, // width of image
                39, // height of image
                100, // posX on screen
                10, // posY on screen
                23, // SizeX of image                   
                19.5); // sizeY of image

        setupText();
                
        if(canInteract == true)
        {
                context.drawImage(spriteSheet,
                    8, // file posX
                    797, // file posY
                    102, // width of image
                    40, // height of image
                    1100, // posX on screen
                    615, // posY on screen
                    100, // SizeX of image                   
                40); // sizeY of image
        }


         context.drawImage(spriteSheet,
                        1, // file posX
                        537, // file posY
                        61, // width of image
                        55, // height of image
                        1300, // posX on screen
                        605, // posY on screen
                        61, // SizeX of image                   
                        55); // sizeY of image
                    
            animate();  
            displayTool();     
            clock();
    }
    else
    {
        context.drawImage(busImage,
                0, // file posX
                0, // file posY
                785, // width of image
                441, // height of image
                busTransition, // posX on screen
                0, // posY on screen
                mapScaledWidth, // SizeX of image                   
                mapScaledHeight); // sizeY of image
       
    }
}

function GameObject(spritesheet, x, y, width, height) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
 
}

function PlayerInput(input) 
{
    this.action = input; 
}
let offsetX = 0;
let offsetY = 103;

function drawFrame(frameX, frameY, canvasX, canvasY)
{
    context.drawImage(spriteSheet, // texture
        (frameX * playerWidth) + offsetX, // texture x
        (frameY * playerHeight) + offsetY, // texture y
        playerWidth, // height
        playerHeight, // width
        canvasX, // canvas x pos
        canvasY, // canvas y pos
        playerScaledWidth,
        playerScaledHeight);
}

function getCursorPosition(canvas, event) 
{
    const rect = canvas.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top

    

    if(x > 470 && x < 548 && y > 595 && y < 673)
    {
        shovel = true;
        hoe = false;
        scythe = false;
        wateringCan = false;
        seed = false;
        crop = false;
        hand = false;
    }
    if(x > 548 && x < 626 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = true;
        scythe = false;
        wateringCan = false;
        seed = false;
        crop = false;
        hand = false;
    }
    if(x > 626 && x < 704 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = false;
        scythe = true;
        wateringCan = false;
        seed = false;
        crop = false;
        hand = false;
        
    }
    if(x > 704 && x < 782 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = true;
        seed = false;
        crop = false;
        hand = false;
    } 
    if(x > 782 && x < 860 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = false;
        seed = true;
        crop = false;
        hand = false;
    }
    if(x > 860 && x < 938 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = false;
        seed = false;
        crop = true;
        hand = false;
    } 
    if(x > 938 && x < 1034 && y > 595 && y < 673)
    {
        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = false;
        seed = false;
        crop = false;
        hand = true;
    } 

    if(x > 1300 && x < 1361 && y > 605 && y < 670)
    {
        if(info == false)
        {
            info = true;
        }
        else
        {
            info = false;
        }
    }

    if(x > 1100 && x < 1200 && y > 615 && y < 655)
    {
        console.log(interacting);
        interacting = true;

        setTimeout(function()
        {
            interacting = false;
            offsetX = 0;
            offsetY = 103;
            playerWidth = 39;
            if(seed || scythe)
            {
                controller = 0;
            }
        }, 2000);
    }
}

function input(event)
{
    if(event.type == "keydown")
    {
        switch(event.keyCode)
        {
            case 37: // Left Arrow
            playerInput = new PlayerInput("left");
            break;
        case 38: // Up Arrow
            playerInput = new PlayerInput("up");
            break; 
        case 39://Right Arrow
            playerInput = new PlayerInput("right");
            break; 
        case 40: // Down Arrow
            playerInput = new PlayerInput("down");
            break;
        case 32: // Down Arrow
        playerInput = new PlayerInput("space");
            break;
        }
    }
    else
    {
        playerInput = new PlayerInput("None");
    }
    if(event.type == "keyup")
    {
        switch(event.keyCode)
        {
            case 32:
                interacting = false;
                offsetX = 0;
                offsetY = 103;
                playerWidth = 39;
                if(seed || scythe)
                {
                    controller = 0;
                }
                break;
                case 49:
                   shovel = true;
                   hoe = false;
                   scythe = false;
                   wateringCan = false;
                   seed = false;
                   crop = false;
                   hand = false;
                    break;
                    case 50:
                shovel = false;
                   hoe = true;
                   scythe = false;
                   wateringCan = false;
                   seed = false;
                   crop = false;
                   hand = false;
                    break;
                    case 51:
                        shovel = false;
                        hoe = false;
                        scythe = true;
                        wateringCan = false;
                        seed = false;
                        crop = false;
                        hand = false;
                    break;
                    case 52:
                        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = true;
        seed = false;
        crop = false;
        hand = false;
                    break;
                    case 53:
                        shovel = false;
        hoe = false;
        scythe = false;
        wateringCan = false;
        seed = true;
        crop = false;
        hand = false;
                    break;
                    case 54:
                        shovel = false;
                        hoe = false;
                        scythe = false;
                        wateringCan = false;
                        seed = false;
                        crop = true;
                        hand = false;
                    break;
                    case 55:
                        shovel = false;
                        hoe = false;
                        scythe = false;
                        wateringCan = false;
                        seed = false;
                        crop = false;
                        hand = true;
                    break;
        }
    }

    if(event.type =="mousedown" || event.type == "touchstart")
    {
        getCursorPosition(canvas, event);
    }
}

let joyStick = nipplejs.create({
    zone: document.getElementById('joystick'),
    mode: 'static',
    size: '40',
    position: {left: '50%', top: '50%'},
    restOpacity: 100,
    color: 'black',
});

    joyStick.on('dir:up', function(event, data)
    {
        playerInput = new PlayerInput("up");

    });

    joyStick.on('dir:down', function(event, data)
    {
        playerInput = new PlayerInput("down");

    });

    joyStick.on('dir:left', function(event, data)
    {
        playerInput = new PlayerInput("left");

    });

    joyStick.on('dir:right', function(event, data)
    {
        playerInput = new PlayerInput("right");

    });

    joyStick.on('end', function (event, data)
    {
        playerInput = new PlayerInput("None");
    });

let speed = 6;
function move()
{
 

    if(playerInput.action == "up")
    {
        if(player.y > 0)
        {
            player.y -= speed;
        }
        direction = 1;
        playerWidth = 36;
    }
    if(playerInput.action == "down")
    {

        if(player.y < mapScaledHeight - 100)
        {
            player.y += speed;
        }
        direction = 0;
        playerWidth = 36;
    }
    if(playerInput.action == "left")
    {

        if(player.x > 0)
        {
            player.x -= speed;
        }
        direction = 2;
        playerWidth = 42;
    }
    if(playerInput.action == "right")
    {

        if(player.x < mapScaledWidth - 50)
        {
            player.x += speed;
        }
        direction = 3;
        playerWidth = 42;
    }
    if(playerInput.action == "space")
    {
        interacting = true;
    }
}

let direction = 0;
let currentLoopIndex = 0;
let frameCount = 0;
const walkLoop = [0, 1, 2];
let frameLimit = 10;

function animate()
{
    if(interacting == false)
    {
        frameLimit = 10;
    }
    else
    {
        frameLimit = 25;
    }
    if(playerInput.action != "None")
    {
        frameCount++;

        if(frameCount >= frameLimit)
        {
            frameCount = 0;
            currentLoopIndex ++;

            if(currentLoopIndex >=  walkLoop.length)
            {
                currentLoopIndex = 0;
            }
        }
    }
    else
    {
        currentLoopIndex = 0;
    }
    
    if(sleeping == true)
    {
        context.drawImage(spriteSheet,
            311, // file posX
            259, // file posY
            41, // width of image
            67, // height of image
            385, // posX on screen
            295, // posY on screen
            41 * playerScale, // SizeX of image                   
            67 * playerScale); // sizeY of image
    }
    else
    {
        if(interacting == false)
        {
            drawFrame(walkLoop[currentLoopIndex], direction, player.x, player.y );   
        }
        else(interacting == true)
        {
            drawFrame(walkLoop[currentLoopIndex], direction, player.x, player.y );   
        }
    }
    
     
}

function displayTool()
{
    context.drawImage(spriteSheet,
        toolPosX, // file posX
        toolPosY, // file posY
        toolWidthX, // width of image
        toolWidthY, // height of image
        player.x + toolOffsetX, // posX on screen
        player.y + toolOffsetY, // posY on screen
        toolWidthX, // SizeX of image                   
        toolWidthY); // sizeY of image

    if(shovel == true)
    {
        toolPosX = 232;
        toolPosY = 0;
        toolWidthX = 40;
        toolWidthY = 41;
        toolOffsetX = 15;
        toolOffsetY = 25;

    }
    else if(hoe == true)
    {
        toolPosX = 235;
        toolPosY = 83;
        toolWidthX = 40;
        toolWidthY = 41;
        toolOffsetX = 15;
        toolOffsetY = 25;
    }
    else if(scythe == true)
    {
        toolPosX = 237;
        toolPosY = 125;
        toolWidthX = 40;
        toolWidthY = 41;
        toolOffsetX = 15;
        toolOffsetY = 25;

    }
    else if(wateringCan == true)
    {
        if(wateringCanFull == false)
        {
            toolPosX = 292;
            toolPosY = 2;
            toolWidthX = 51;
            toolWidthY = 34;
            toolOffsetX = 0;
            toolOffsetY = 40;
        }
        else
        {
            toolPosX = 292;
            toolPosY = 37;
            toolWidthX = 51;
            toolWidthY = 34;
            toolOffsetX = 0;
            toolOffsetY = 40;
        }
    }
    else if(seed == true)
    {
        toolPosX = 3;
        toolPosY = 3;
        toolWidthX = 27;
        toolWidthY = 28;
        toolOffsetX = 20;
        toolOffsetY = 40;
    }
    else if(crop == true)
    {
        toolPosX = 48;
        toolPosY = 56;
        toolWidthX = 16;
        toolWidthY = 32;
        toolOffsetX = 25;
        toolOffsetY = 40;
    }
    else if(hand == true)
    {
        toolPosX = 0;
        toolPosY = 0;
        toolWidthX = 0;
        toolWidthY = 0;
        toolOffsetX = 0;
        toolOffsetY = 0;
    }

    if(info == true)
    {
        context.drawImage(spriteSheet,
            7, // file posX
            608, // file posY
            200, // width of image
            128, // height of image
            1020, // posX on screen
            500, // posY on screen
            270, // SizeX of image                   
            160); // sizeY of image
    }
}

function interact()
{
    console.log("X: " + player.x + ", Y: " + player.y);
    if(!inside)
    {
        if(shovel == true)
        {
            digSoil();
        }
        if(hoe == true)
        {
            tillSoil();
        }
        if(seed == true)
        {
            if(currentSeeds > 0)
            {
                plantSeed();
            }
        }
        if(wateringCan == true && wateringCanFull == true)
        {
            waterSeed();
        }
        if(wateringCan == true)
        {
            collectWater();
        }

        if(scythe == true)
        {
            harvestCrop();
        }

        goInside();
        if(coins >= coinsToWin && time.getHours() == timeToWin)
        {
            escape();
        }
    }
    else
    {
        goOutside();
        depositCrops();
        sleep();
      
    }
}

function collectWater()
{
    if(player.x > 880 && player.x < 915 && player.y > 400 && player.y < 450)
    {
        direction = 2;
        offsetX = 151;
        offsetY = 115;
        playerWidth = 51;
        
        toolOffsetX = -20;

        setTimeout(function()
        {
            wateringCanFull = true;
        }, 2000);
    }
}

function digSoil()
{
    // FARM 1
    if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
    {
        canInteract = true;
        direction = 2;
        offsetX = 151;
        offsetY = 115;
        playerWidth = 51;

        toolOffsetX = -10;

        setTimeout(function()
        {
            farmLand1FileXPos = 132;
            farmLand1Width = 11;
            farmLand1XPos = 395.5;
        }, 2000);
    }
    // FARM 2
    if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
    {
        direction = 2;
        offsetX = 151;
        offsetY = 115;
        playerWidth = 51;

        toolOffsetX = -10;

        setTimeout(function()
        {
            farmLand2FileXPos = 132;
            farmLand2Width = 11;
            farmLand2XPos = 229.5;
        }, 2000);
    }

    // FARM 3
    if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
    {
        direction = 2;
        offsetX = 151;
        offsetY = 115;
        playerWidth = 51;

        toolOffsetX = -10;

        setTimeout(function()
        {
            farmLand3FileXPos = 132;
            farmLand3Width = 11;
            farmLand3XPos = 229.5;

        }, 2000);
    }
}

function tillSoil()
{
    // FARM 1
    if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
    {
        if(farmLand1FileXPos == 132)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand1FileXPos = 144;
                farmLand1Width = 11;
                farmLand1XPos = 395.5;

            }, 2000);
        }
    }

    // FARM 2
    if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
    {
        if(farmLand2FileXPos == 132)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand2FileXPos = 144;
                farmLand2Width = 11;
                farmLand2XPos = 229.5;

            }, 2000);
        }
    }

    // FARM 3
    if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
    {
        if(farmLand3FileXPos == 132)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand3FileXPos = 144;
                farmLand3Width = 11;
                farmLand3XPos = 229.5;

            }, 2000);
        }
    }
}

let controller = 1;

function plantSeed()
{
    
    // FARM 1
    if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
    {
        if(farmLand1FileXPos == 144)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand1FileXPos = 158;
                farmLand1Width = 12;
                farmLand1XPos = 393.5;
                if(currentSeeds > 0)
                {
                    if(controller == 0)
                    {
                        currentSeeds--;
                        controller++;
                        localStorage.setItem("Seeds", currentSeeds);
                    }  
                }
                
            }, 2000);
        }
    }

    // FARM 2
    if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
    {
        if(farmLand2FileXPos == 144)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand2FileXPos = 158;
                farmLand2Width = 12;
                farmLand2XPos = 227.5;
                if(currentSeeds > 0)
                {
                    if(controller == 0)
                    {
                        currentSeeds--;
                        controller++;
                        localStorage.setItem("Seeds", currentSeeds);
                    }  
                }

            }, 2000);
        }
    }

    // FARM 3
    if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
    {
        if(farmLand3FileXPos == 144)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;

            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand3FileXPos = 158;
                farmLand3Width = 12;
                farmLand3XPos = 227.5;

                if(currentSeeds > 0)
                {
                    if(controller == 0)
                    {
                        currentSeeds--;
                        controller++;
                        localStorage.setItem("Seeds", currentSeeds);
                    }  
                }
            }, 2000);
        }
    }

    
}

function waterSeed()
{

    // FARM 1
    if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
    {
        if(farmLand1FileXPos == 158)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;

            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand1Watered = true;
                wateringCanFull = false;
            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }

    // FARM 2
    if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
    {
        if(farmLand2FileXPos == 158)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;

            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand2Watered = true;
                wateringCanFull = false;
            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }

    // FARM 3
    if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
    {
        if(farmLand3FileXPos == 158)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;

            toolOffsetX = -10;

            setTimeout(function()
            {
                farmLand3Watered = true;
                wateringCanFull = false;

            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }
}

function growSeed()
{
    if(farmLand1Watered == true)
    {
        farmLand1FileXPos = 180;
        farmLand1Width = 21;
        farmLand1XPos = 384.5;
        farmLand1Watered = false;
    }

    if(farmLand2Watered == true)
    {
        farmLand2FileXPos = 180;
        farmLand2Width = 21;
        farmLand2XPos = 218.5;
        farmLand2Watered = false;
    }

    if(farmLand3Watered == true)
    {
        farmLand3FileXPos = 180;
        farmLand3Width = 21;
        farmLand3XPos = 215.5;
        farmLand3Watered = false;
    }
}

function harvestCrop()
{
      // FARM 1
      if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
      {
        if(farmLand1FileXPos == 180)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
    
            toolOffsetX = -10;
    
            setTimeout(function()
            {
                farmLand1FileXPos = 120;
                farmLand1Width = 11;
                farmLand1XPos = 395.5;
                if(controller == 0)
                {
                    currentCrops++;
                    controller++;
                    currentSeeds += 2;
                    localStorage.setItem("CurrentCrops", currentCrops);
                    localStorage.setItem("CurrentSeeds", currentSeeds);
                }           
            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }
  
      // FARM 2
      if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
      {
        if(farmLand2FileXPos == 180)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
    
            toolOffsetX = -10;
    
            setTimeout(function()
            {
                farmLand2FileXPos = 120;
                farmLand2Width = 11;
                farmLand2XPos = 229.5;
                if(controller == 0)
                {
                    currentCrops++;
                    controller++;
                    currentSeeds += 2;
                    localStorage.setItem("CurrentCrops", currentCrops);
                    localStorage.setItem("CurrentSeeds", currentSeeds);
                }           
            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }
  
      // FARM 3
      if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
      {
        if(farmLand3FileXPos == 180)
        {
            direction = 2;
            offsetX = 151;
            offsetY = 115;
            playerWidth = 51;
    
            toolOffsetX = -10;
    
            setTimeout(function()
            {
                farmLand3FileXPos = 120;
                farmLand3Width = 11;
                farmLand3XPos = 229.5;
                if(controller == 0)
                {
                currentCrops++;
                controller++;
                currentSeeds += 2;
                localStorage.setItem("CurrentCrops", currentCrops);
                localStorage.setItem("CurrentSeeds", currentSeeds);
                }           
            }, 2000);
        }
        canInteract = true;
    }
    else
    {
        canInteract = false;
    }
    
}

function goInside()
{
    if(player.x > 1190 && player.x < 1265 && player.y > 55 && player.y < 80)
    {
        setTimeout(function()
        {
            inside = true;
            player.x = 692;
            player.y = 486;

        }, 750);
    }
}

function goOutside()
{
    if(player.x > 662 && player.x < 720 && player.y > 468 && player.y < 498)
        {
            setTimeout(function()
            {
                direction = 0;
                inside = false;
                player.x = 1230;
                player.y = 72;

            }, 750);
        }
}

function depositCrops()
{
    if(player.x > 915 && player.x < 950 && player.y > 408 && player.y < 462)
    {
        storedCrops = currentCrops;
        localStorage.setItem("StoredCrops",storedCrops);

        setTimeout(function()
        {
            currentCrops = 0;
            localStorage.setItem("CurrentCrops", currentCrops);

        }, 500);
    }
}

function sleep()
{
    if(player.x > 400 && player.x < 450 && player.y > 260 && player.y < 320)
    {
        sleeping = true;

        if(sleeping == false)
        {
            minutePlusser = 5;
            player.x = 430;
            direction = 3;
        }
    }
}

function escape()
{
    if(player.x > -10 && player.x < 25 && player.y > 35 && player.y < 80)
    {
        escaped = true;
    }
}

function displayFarmLand()
{
    context.drawImage(spriteSheet,
        farmLand1FileXPos, // file posX
        farmLand1FileYPos, // file posY
        farmLand1Width, // width of image
        farmLandHeight, // height of image
        farmLand1XPos, // posX on screen
        farmLand1YPos, // posY on screen
        farmLand1ScaledWidth, // SizeX of image                   
        farmLandScaledHeight); // sizeY of image

    context.drawImage(spriteSheet,
        farmLand2FileXPos, // file posX
        farmLand2FileYPos, // file posY
        farmLand2Width, // width of image
        farmLandHeight, // height of image
        farmLand2XPos, // posX on screen
        farmLand2YPos, // posY on screen
        farmLand2ScaledWidth, // SizeX of image                   
        farmLandScaledHeight); // sizeY of image

    context.drawImage(spriteSheet,
        farmLand3FileXPos, // file posX
        farmLand3FileYPos, // file posY
        farmLand3Width, // width of image
        farmLandHeight, // height of image
        farmLand3XPos, // posX on screen
        farmLand3YPos, // posY on screen
        farmLand3ScaledWidth, // SizeX of image                   
        farmLandScaledHeight); // sizeY of image
}

let playerName = "Farmer";

function setupText()
{
    context.fillStyle = "rgba(255, 255, 255, 0.3)";
    context.fillRect(10, 5, 160, 60);
    
    context.fillStyle = 'black';
    context.strokeRect(10, 5, 160, 60);
    context.font = "13px Arial";
    context.fillText("x " + currentSeeds, 828, 615);

    context.font = "13px Arial";
    context.fillText("x " + currentCrops, 913, 615);

    context.font = "25px Arial";
    context.fillText("Day " + day, 20, 30);

    context.font = "30px Arial Bold";
    context.fillText(coins, 130, 29);

    context.font = "30px Arial Bold";
    context.fillText(playerName, player.x, player.y - 20);

    if(inside == true)
    {
        context.font = "25px Arial Bold";
        context.fillText("x " + storedCrops, 975, 500);
    }
}

let time = new Date(2000, 01, 01, 06, 55, 00);
let previousSecond = 0;
let hourCounter = 0;


function clock()
{
    let date = new Date();

    context.font = "20px Arial";

    if (time.getHours() < 10 && time.getMinutes() < 10)
    {
        context.fillText("0" + time.getHours()
        +":0"+time.getMinutes(), 20, 55);
       
    }
    else if (time.getHours() < 10)
    {
        context.fillText("0" + time.getHours()
        +":"+time.getMinutes(), 20, 55);
      
    }
    else if (time.getMinutes() < 10)
    {
        context.fillText(time.getHours()
        +":0"+time.getMinutes(), 20, 55);
  
    }
    else
    {
        context.fillText(time.getHours()
        +":"+time.getMinutes(), 20, 55);
      
    
    }

    if (date.getSeconds() > previousSecond || (date.getSeconds() == 0 && previousSecond == 59))
    {
        time.setMinutes(time.getMinutes() + minutePlusser);
        previousSecond = date.getSeconds();
    }
}

function updateTime()
{
    if(time.getHours() == 6 && time.getMinutes() == 00)
    {
        if(hourCounter == 0)
        {
            minutePlusser = 5;
            getCoins();
            growSeed();
            day++;
            hourCounter++;
            sleeping = false;
            minutePlusser = 5;
         
        }
    }
    else if(time.getHours() == 7 && time.getMinutes() == 00)
    {
 
    }
    else
    {
        hourCounter = 0;
    }

    if(time.getHours() == 6)
    {
        mapYFilePos = 0;
    }

    if(time.getHours() == 16)
    {
        mapYFilePos = 442;
    }

    if(time.getHours() == 20)
    {
        mapYFilePos = 883;
    }

    if(time.getMinutes() == 55)
    {
        playerName = localStorage.getItem('userName');
       
        localStorage.setItem("Coins", coins);
        localStorage.setItem("Current Crops", currentCrops);
        
        
        localStorage.setItem("StoredCrops", storedCrops);
        
    }
}

function getCoins()
{
    console.log("CurrentCrops" + currentCrops);
    console.log("StoredCrops" + storedCrops); 
    coins += (storedCrops * 5);
    console.log("Coins" + coins);
    console.log("StoredCrops" + storedCrops);

    storedCrops = 0;
    localStorage.setItem("Coins", coins);
    localStorage.setItem("StoredCrops",storedCrops);
}

function displayInteractMenu()
{
    // WATER

    if(player.x > 880 && player.x < 915 && player.y > 400 && player.y < 450)
    {
        canInteract = true;
    }
    // FARM 1
    else if(player.x > 390 && player.x < 422 && player.y > 80 && player.y < 130)
    {
        canInteract = true;
    }
    // FARM 2
    else if(player.x > 234 && player.x < 256 && player.y > 80 && player.y < 130)
    {
        canInteract = true;
    }
    // FARM 3
    else if(player.x > 234 && player.x < 256 && player.y > 320 && player.y < 369)
    {
        canInteract = true;
    }
    // OUTSIDE DOOR
    else  if(player.x > 1190 && player.x < 1265 && player.y > 55 && player.y < 80)
    {
        canInteract = true;
    }
    // INSIDE DOOR      
    else if(player.x > 662 && player.x < 720 && player.y > 468 && player.y < 498 && inside == true)
    {
        canInteract = true;
    }
    // DEPOSIT
    else  if(player.x > 915 && player.x < 950 && player.y > 408 && player.y < 462 && inside == true)
    {
        canInteract = true;
    }
    // SLEEP
    else if(player.x > 400 && player.x < 450 && player.y > 260 && player.y < 320 && inside == true)
    {
        canInteract = true;
    }
       // BUS STOP
    else if(player.x > -10 && player.x < 25 && player.y > 35 && player.y < 80)
    {
        
        if(coins >= coinsToWin && time.getHours() == timeToWin)
        {
            console.log("BUS");
            canInteract = true;
        }
    }
    else 
    {
        canInteract = false;
    }
}





